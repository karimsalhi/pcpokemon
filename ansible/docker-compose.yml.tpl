version: "3.8"

services:
  client:
    image: $ECR_PATH:latest
    ports:
      - "80:5173"
      