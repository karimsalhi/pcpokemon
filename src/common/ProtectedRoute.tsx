import { useContext } from "react";
import UserContext from "../contexts/userContext";
import { Navigate, Outlet } from "react-router-dom";

export default function ProtectedRoute() {
  const { userData } = useContext(UserContext);

  if (
    !userData.trainerId ||
    !userData.accessToken ||
    userData.accessToken === "" ||
    userData.trainerId === 0
  ) {
    return <Navigate to="/login" replace />;
  }
  return <Outlet />;
}
