import { useContext } from "react";
import UserContext from "../contexts/userContext";

export default function CheckAuthenticated() {
  const { userData } = useContext(UserContext);

  if (
    !userData.trainerId ||
    !userData.accessToken ||
    userData.accessToken === "" ||
    userData.trainerId === 0
  ) {
    return false;
  }
  return true;
}
