import { RouterProvider, createBrowserRouter } from "react-router-dom";
import ProtectedRoute from "./common/ProtectedRoute";
import BoxeDetails from "./components/BoxeDetails/BoxeDetails";
import Boxes from "./components/Boxes/Boxes";
import Home from "./components/Home/Home";
import Layout from "./components/Layout/Layout";
import Login from "./components/Login/Login";
import SignUp from "./components/SignUp/SignUp";
import { UserContextProvider } from "./contexts/userContext";
import NewBox from "./components/Boxes/NewBox/NewBox";
import APropos from "./components/APropos/APropos";
import NewPokemon from "./components/BoxeDetails/NewPokemon/NewPokemon";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/sign-up",
        element: <SignUp />,
      },
      {
        path: "/a-propos",
        element: <APropos />,
      },
      {
        path: "/",
        element: <ProtectedRoute />,
        children: [
          {
            path: "",
            element: <Boxes />,
          },
          {
            path: "new-box",
            element: <NewBox />,
          },
          {
            path: "/home",
            element: <Home />,
          },
          {
            path: "boxes/:boxeId",
            element: <BoxeDetails />,
          },
          {
            path: "boxes/:boxeId/new-pokemon",
            element: <NewPokemon />,
          },
        ],
      },
    ],
  },
]);
export default function App() {
  return (
    <>
      <UserContextProvider>
        <RouterProvider router={router} />
      </UserContextProvider>
    </>
  );
}
