import { ReactNode, createContext, useState } from "react";
import { UserType } from "../types/UserType/UserType";

export const defaultUserData: UserType = {
  accessToken: "",
  trainerId: 0,
};

const UserContext = createContext<{
  userData: UserType;
  setUserData: (data: UserType) => void;
}>({
  userData: defaultUserData,
  setUserData: () => undefined,
});

export const UserContextProvider = ({ children }: { children: ReactNode }) => {
  const [userData, setUserData] = useState(defaultUserData);
  return (
    <UserContext.Provider value={{ userData, setUserData }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserContext;
