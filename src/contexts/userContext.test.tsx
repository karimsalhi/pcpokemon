import { render, screen } from '@testing-library/react';
import UserContext, { UserContextProvider, defaultUserData } from './userContext';
import { useContext } from 'react';

const MockChildComponent = () => {
  const userContext = useContext(UserContext);
  return (
    <div>
      <span data-testid="accessToken">{userContext.userData.accessToken}</span>
      <span data-testid="trainerId">{userContext.userData.trainerId}</span>
    </div>
  );
};

test('UserContextProvider sets default values', () => {
  render(
    <UserContextProvider>
      <MockChildComponent />
    </UserContextProvider>
  );

  const accessTokenElement = screen.getByTestId('accessToken');
  const trainerIdElement = screen.getByTestId('trainerId');

  expect(accessTokenElement.textContent).toBe(defaultUserData.accessToken);
  expect(parseInt(trainerIdElement.textContent || '0', 10)).toBe(defaultUserData.trainerId);
});

