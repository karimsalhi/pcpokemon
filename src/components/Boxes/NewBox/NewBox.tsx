import { useContext, useState } from "react";
import { createBoxe } from "../../../services/BoxesService/BoxesService";
import UserContext from "../../../contexts/userContext";
import { useNavigate } from "react-router-dom";

export default function NewBox() {
  const navigate = useNavigate();
  const { userData } = useContext(UserContext);
  const [box, setBox] = useState("");

  async function handleClick(e: { preventDefault: () => void }) {
    e.preventDefault();
    const result = await createBoxe(userData, box);
    if (result.statusCode == 201) {
      navigate("/");
    } else {
      //Handle error
    }
  }
  return (
    <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
      <form className="space-y-6" onSubmit={handleClick}>
        <div className="mt-2">
          <label htmlFor="box" className="sr-only">
            Box Name
          </label>
          <input
            id="box"
            name="box"
            type="text"
            value={box}
            required
            className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            onChange={(e) => setBox(e.target.value)}
          />
        </div>
        <div>
          <button
            type="submit"
            className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          >
            Create box
          </button>
        </div>
      </form>
    </div>
  );
}
