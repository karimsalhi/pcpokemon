import '@testing-library/jest-dom';
import { render, screen, waitFor } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import Boxes from './Boxes';
import { getBoxes } from '../../services/BoxesService/BoxesService';

jest.mock('../../services/BoxesService/BoxesService');

describe('Boxes Component', () => {
  const mockGetBoxes = getBoxes as jest.MockedFunction<typeof getBoxes>;

  it('renders Boxes component correctly with mock data', async () => {
    const mockBoxesData = {
      statusCode: 200,
      boxes: [
        { id: 1, name: 'Box 1' },
        { id: 2, name: 'Box 2' },
        { id: 3, name: 'Box 3' },
      ],
    };

    mockGetBoxes.mockResolvedValueOnce(mockBoxesData);

    render(
      <Router>
        <Boxes />
      </Router>
    );

    expect(screen.getByText('Create a new box')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByText('Box 1')).toBeInTheDocument();
      expect(screen.getByText('Box 2')).toBeInTheDocument();
      expect(screen.getByText('Box 3')).toBeInTheDocument();
    });
  });

  it('renders "Create a new box" button and handles click event', async () => {
    const mockBoxesData = {
      statusCode: 200,
      boxes: [
        { id: 1, name: 'Box 1' },
        { id: 2, name: 'Box 2' },
        { id: 3, name: 'Box 3' },
      ],
    };

    mockGetBoxes.mockResolvedValueOnce(mockBoxesData);

    render(
      <Router>
        <Boxes />
      </Router>
    );

    const createNewBoxButton = screen.getByText('Create a new box');

    expect(createNewBoxButton).toBeInTheDocument();

  });
});
