import { useContext, useEffect, useState } from "react";
import { getBoxes } from "../../services/BoxesService/BoxesService";
import UserContext from "../../contexts/userContext";
import { Boxe } from "../../types/BoxesType/BoxesType";

import { NavLink } from "react-router-dom";

export default function Boxes() {
  const { userData } = useContext(UserContext);
  const [boxes, setBoxes] = useState<Boxe[]>([]);
  useEffect(() => {
    const fetchBoxes = async () => {
      const data = await getBoxes(userData);
      if (data.statusCode === 200) {
        const boxes = data.boxes as Boxe[];
        setBoxes(boxes);
      }
    };
    fetchBoxes();
  }, [userData]);

  return (
    <>
      <NavLink to={"/new-box"}>
        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
          Create a new box
        </button>
      </NavLink>
      <ul
        role="list"
        className="divide-y divide-gray-100 grid grid-cols-3 gap-4 content-center"
      >
        {boxes.map((boxe, key) => (
          <div className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <NavLink key={key} to={`/boxes/${boxe.id}`}>
              <div className="flex flex-col items-center pb-10">
                <img
                  className="w-24 h-24 mb-3 rounded-full shadow-lg"
                  src="https://resize-gulli.jnsmedia.fr/r/890,__ym__/img//var/jeunesse/storage/images/gulli/chaine-tv/dessins-animes/pokemon/pokemon/pikachu/26571681-1-fre-FR/Pikachu.jpg"
                  alt="Bonnie image"
                />
                <h5 className="mb-1 text-xl font-medium text-gray-900 dark:text-white">
                  {boxe.name}
                </h5>
                <span className="text-sm text-gray-500 dark:text-gray-400">
                  Boxe
                </span>
              </div>
            </NavLink>
          </div>
        ))}
      </ul>
    </>
  );
}
