import { NavLink } from "react-router-dom";
import CheckAuthenticated from "../../common/checkAuthenticated";
import { useContext } from "react";
import UserContext, { defaultUserData } from "../../contexts/userContext";

export default function Header() {
  const isLoggedIn = CheckAuthenticated();
  const { setUserData } = useContext(UserContext);
  return (
    <header className="bg-white">
      <nav
        className="mx-auto flex max-w-7xl items-center justify-between p-6 lg:px-8"
        aria-label="Global"
      >
        <div className="flex lg:flex-1">
          <NavLink to={"/"} className="-m-1.5 p-1.5">
            <span className="sr-only">Pikachu</span>
            <img className="h-8 w-auto" src="https://resize-gulli.jnsmedia.fr/r/890,__ym__/img//var/jeunesse/storage/images/gulli/chaine-tv/dessins-animes/pokemon/pokemon/pikachu/26571681-1-fre-FR/Pikachu.jpg" alt="" />
          </NavLink>
        </div>
        <div className="flex lg:flex-1">
          <NavLink to={"a-propos"} className="-m-1.5 p-1.5">
            A propos
          </NavLink>
        </div>
        {isLoggedIn && (
          <div className="flex gap-x-12">
            <NavLink
              to={"/"}
              className="text-sm font-semibold leading-6 text-gray-900"
            >
              Boxes
            </NavLink>
            <NavLink
              to={"/home"}
              className="text-sm font-semibold leading-6 text-gray-900"
            >
              Exchanges
            </NavLink>
            <NavLink
              onClick={() => setUserData(defaultUserData)}
              to={"/login"}
              className="text-sm font-semibold leading-6 text-gray-900"
            >
              Log out
            </NavLink>
          </div>
        )}
        {!isLoggedIn && (
          <div className="hidden lg:flex lg:flex-1 lg:gap-x-12 lg:justify-end">
            <NavLink
              to={"/login"}
              className="text-sm font-semibold leading-6 text-gray-900"
            >
              Log in <span aria-hidden="true">&rarr;</span>
            </NavLink>
            <NavLink
              to={"/sign-up"}
              className="text-sm font-semibold leading-6 text-gray-900"
            >
              Sign up <span aria-hidden="true">&rarr;</span>
            </NavLink>
          </div>
        )}
      </nav>
    </header>
  );
}
