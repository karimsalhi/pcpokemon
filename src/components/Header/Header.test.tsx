import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Header from './Header';

test('renders Header component', () => {
  render(
    <BrowserRouter>
      <Header />
    </BrowserRouter>
  );

  const aProposLink = screen.getByText('A propos');
  expect(aProposLink).toBeInTheDocument();

});
