import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { MemoryRouter } from 'react-router-dom';
import Layout from './Layout';

test('renders Layout component', () => {
  const { getByText } = render(
    <MemoryRouter>
      <Layout />
    </MemoryRouter>
  );

  const headerElement = getByText(/Pikachu/i); 
  expect(headerElement).toBeInTheDocument();
});

