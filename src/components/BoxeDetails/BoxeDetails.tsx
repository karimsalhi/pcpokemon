import { useContext, useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { Pokemon } from "../../types/PokemonType/PokemonType";
import UserContext from "../../contexts/userContext";

import { getBoxeById } from "../../services/BoxesService/BoxesService";

export default function BoxeDetails() {
  const { userData } = useContext(UserContext);
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [boxeName, setBoxeName] = useState("");
  const params = useParams();
  useEffect(() => {
    // const fetchBoxes = async () => {
    //   const data = await getPokemons(userData, params.boxeId);
    //   if (data.statusCode === 200) {
    //     const pokemons = data.pokemons as Pokemon[];
    //     setPokemons(pokemons);
    //   }
    // };
    const getBoxe = async () => {
      const data = await getBoxeById(userData, params.boxeId);
      if (data.statusCode === 200) {
        const boxeName = data.boxe?.name;
        const pokemons = data.boxe?.pokemons as Pokemon[];
        setPokemons(pokemons);
        setBoxeName(boxeName);
      }
    };
    getBoxe();
  }, []);

  return (
    <>
      <h1 className="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl ">
        {boxeName}
      </h1>
      <NavLink to={`/boxes/${params.boxeId}/new-pokemon`}>
        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
          Create a new pokemon
        </button>
      </NavLink>
      <ul
        role="list"
        className="divide-y divide-gray-100 grid grid-cols-3 gap-4 content-center"
      >
        {pokemons.map((pokemon, key) => (
          <div className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <NavLink key={key} to={`/pokemons/${pokemon.id}`}>
              <div className="flex flex-col items-center pb-10">
                <img
                  className="w-24 h-24 mb-3 rounded-full shadow-lg"
                  src="https://resize-gulli.jnsmedia.fr/r/890,__ym__/img//var/jeunesse/storage/images/gulli/chaine-tv/dessins-animes/pokemon/pokemon/pikachu/26571681-1-fre-FR/Pikachu.jpg"
                  alt="Bonnie image"
                />
                <h5 className="mb-1 text-xl font-medium text-gray-900 dark:text-white">
                  {pokemon.name}
                </h5>
                <span className="text-sm text-gray-500 dark:text-gray-400">
                  Specie: {pokemon.species}
                </span>
                <span className="text-sm text-gray-500 dark:text-gray-400">
                  Level: {pokemon.level}
                </span>
                <span className="text-sm text-gray-500 dark:text-gray-400">
                  Gender: {pokemon.genderTypeCode}
                </span>
                <span className="text-sm text-gray-500 dark:text-gray-400">
                  Shiny: {pokemon.isShiny ? "yes" : "no"}
                </span>
              </div>
            </NavLink>
          </div>
        ))}
      </ul>
    </>
  );
}
