import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import BoxeDetails from "./BoxeDetails";
import '@testing-library/jest-dom';

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({
    boxeId: "123", 
  }),
}));

describe("BoxeDetails Component", () => {
  it("renders component without crashing", async () => {
    render(
      <BrowserRouter>
        <BoxeDetails />
      </BrowserRouter>
    );

    expect(screen.getByText((content) => content.includes("Create a new pokemon"))).toBeInTheDocument();
  });

  it("renders 'Create a new pokemon' button", () => {
    render(
      <BrowserRouter>
        <BoxeDetails />
      </BrowserRouter>
    );

    expect(screen.getByText(/create a new pokemon/i)).toBeInTheDocument();
  });
});
