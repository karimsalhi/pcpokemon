import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import APropos from './APropos';

test('renders APropos component', () => {
  render(<APropos />);

  const headingElement = screen.getByText('Karim SALHI');
  expect(headingElement).toBeInTheDocument();
});
