import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import Home from "./Home";

test("renders Home component", () => {
  render(<Home />);
  const homeElement = screen.getByText(/Home/i);
  expect(homeElement).toBeInTheDocument();
});
