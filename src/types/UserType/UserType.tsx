export type UserType = {
  trainerId: number;
  accessToken: string;
};
