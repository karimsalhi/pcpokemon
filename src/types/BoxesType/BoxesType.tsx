import { Pokemon } from "../PokemonType/PokemonType";

export type Boxes = {
  boxes: Boxe[];
};

export type Boxe = {
  id: number;
  name: string;
  pokemons?: Pokemon[];
};
