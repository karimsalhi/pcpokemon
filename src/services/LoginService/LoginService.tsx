interface ILoginResult {
  accessToken?: string;
  trainerId?: number;
  errorMessage?: string;
  statusCode: number;
}

export const LoginService = async (
  login: string,
  password: string
): Promise<ILoginResult> => {
  const path = "http://localhost:8000/login";
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ login, password }),
  };

  try {
    const result = await fetch(path, requestOptions);
    const data = await result.json();

    if (data.accessToken) {
      return {
        accessToken: data.accessToken,
        trainerId: data.trainerId,
        statusCode: 200,
      };
    } else {
      return { statusCode: 401, errorMessage: "Wrong username or password" };
    }
  } catch (e) {
    return { statusCode: 404 };
  }
};
