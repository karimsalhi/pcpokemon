import { CreatePokemon } from "../../types/PokemonType/PokemonType";
import { UserType } from "../../types/UserType/UserType";

export async function getPokemons(
  userData: UserType,
  boxeId: string | undefined
) {
  const path = `http://localhost:8000/trainers/${userData.trainerId}/boxes/${boxeId}`;
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userData.accessToken}`,
    },
  };

  try {
    const result = await fetch(path, requestOptions);
    const data = await result.json();

    return { statusCode: 200, pokemons: data.pokemons };
  } catch (e) {
    return { statusCode: 400, pokemons: [] };
  }
}

export async function createPokemons(
  userData: UserType,
  pokemon: CreatePokemon
) {
  const path = `http://localhost:8000/pokemons`;
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userData.accessToken}`,
    },
    body: JSON.stringify(pokemon),
  };

  try {
    const result = await fetch(path, requestOptions);
    const data = await result.json();
    console.log(data);

    return { statusCode: 201, id: data.id };
  } catch (e) {
    return { statusCode: 400 };
  }
}
