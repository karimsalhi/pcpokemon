import { Boxe } from "../../types/BoxesType/BoxesType";
import { UserType } from "../../types/UserType/UserType";

interface IBoxesResult {
  statusCode: number;
  boxes?: Boxe[];
}

interface IBoxeResult {
  statusCode: number;
  boxe: Boxe;
}

export const getBoxes = async (userData: UserType): Promise<IBoxesResult> => {
  const path = `http://localhost:8000/trainers/${userData.trainerId}/boxes`;
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userData.accessToken}`,
    },
  };

  try {
    const result = await fetch(path, requestOptions);
    const data = (await result.json()) as Boxe[];

    return { statusCode: 200, boxes: data };
  } catch (e) {
    return { statusCode: 400, boxes: [] };
  }
};

export const createBoxe = async (userData: UserType, boxName: string) => {
  const path = `http://localhost:8000/trainers/${userData.trainerId}/boxes`;
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userData.accessToken}`,
    },
    body: JSON.stringify({
      name: boxName,
    }),
  };

  try {
    const result = await fetch(path, requestOptions);
    const data = await result.json();

    return { statusCode: 201, id: data.id };
  } catch (e) {
    return {
      statusCode: 400,
      errorMessage: "Your request doesn't have the fields expected",
    };
  }
};

export const getBoxeById = async (
  userData: UserType,
  boxeId: string | undefined
): Promise<IBoxeResult> => {
  const path = `http://localhost:8000/trainers/${userData.trainerId}/boxes/${boxeId}`;
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userData.accessToken}`,
    },
  };

  try {
    const result = await fetch(path, requestOptions);
    const data = (await result.json()) as Boxe;

    return { statusCode: 200, boxe: data };
  } catch (e) {
    return { statusCode: 400, boxe: { id: 0, name: "" } };
  }
};
