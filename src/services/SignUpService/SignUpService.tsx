interface ISignUpResult {
  accessToken?: string;
  trainerId?: number;
  errorMessage?: string;
  statusCode: number;
}

export const SignUpService = async (
  firstname: string,
  lastname: string,
  login: string,
  birthDate: string,
  password: string
): Promise<ISignUpResult> => {
  const path = "http://localhost:8000/subscribe";
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      firstName: firstname,
      lastName: lastname,
      login,
      birthDate,
      password,
    }),
  };

  try {
    const result = await fetch(path, requestOptions);
    const data = await result.json();

    if (data.accessToken) {
      return {
        accessToken: data.accessToken,
        trainerId: data.trainerId,
        statusCode: 201,
      };
    } else {
      return {
        statusCode: 400,
        errorMessage: "Your request doesn't have the fields expected",
      };
    }
  } catch (e) {
    return { statusCode: 404 };
  }
};
