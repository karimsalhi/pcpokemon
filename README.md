# Devops part

Go to settings -> CI/CD. Under Variables, define 
`AWS_ACCESS_KEY_ID` `AWS_DEFAULT_REGION` `AWS_SECRET_ACCESS_KEY` and `AWS_SESSION_TOKEN`

Add also `CI_AWS_CF_STACK_NAME`=`Cloudformation-react`


For the deployment of Terraform, you need to run the pipeline manually and set `EXECUTE_TERRAFORM` to `True`

### Group members
- Karim SALHI
- Tony BLARD
