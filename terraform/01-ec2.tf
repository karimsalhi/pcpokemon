resource "aws_key_pair" "permission_key" {
  key_name   = "react"
  public_key = tls_private_key.key.public_key_openssh
}

resource "aws_instance" "my-ec2" {
  ami                        = data.aws_ami.latest_amazon_linux_2023.id
  associate_public_ip_address = true
  instance_type              = var.instance_type
  iam_instance_profile       = "LabInstanceProfile" // As we can't create IAM Role, I'm using an existing role
  security_groups            = [aws_security_group.sg-epita.name]
  key_name                   = aws_key_pair.permission_key.key_name
}

resource "aws_security_group" "sg-epita" {
  name_prefix = "sg_epita"
  description = "SG used in my courses"

  # vpc_id = "vpc-0a99818cd35f033e6"

  # Règle pour autoriser tout le trafic
 egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP flow rule
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS flow rule
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # SSH flow rule
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Chomp remove end character
  } 

}