resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "aws_secretsmanager_secret" "permission_key" {
  name = "permission-secret"
}

resource "aws_secretsmanager_secret_version" "epita" {
  secret_id     = aws_secretsmanager_secret.permission_key.id
  secret_string = tls_private_key.key.private_key_pem
}