# Terraform AWS EC2 Instance Deployment

This Terraform module deploys an EC2 instance on AWS, complete with a security group, key pair, and a secret in AWS Secrets Manager to store the private key.

```bash
aws secretsmanager get-secret-value --secret-id <SECRET-ID> --query 'SecretString' --output text --region us-east-1
```
